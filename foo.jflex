/**
 * This is a simple example of a jflex lexer definition
 * Author: Jackson Nelson
 */

/* Declarations */

import java.util.HashMap;

%%

%class  MyScanner   /* Names the produced java file */
%function nextToken /* Renames the yylex() function */
%type   String      /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}

%{
		private String theSource = "";
		private int currentIndex = 0;
		private HashMap<String,TokenType> lookupTable = new HashMap<String,TokenType>();

		public void SimpleScanner() 
		{
			//this.theSource = toScan;
        
			// put stuff in the lookup table
			// KEYWORDS
			lookupTable.put("program", TokenType.PROGRAM);
			lookupTable.put("var", TokenType.VAR);
			lookupTable.put("array", TokenType.ARRAY);
			lookupTable.put("of", TokenType.OF);
			lookupTable.put("integer", TokenType.INTEGER);
			lookupTable.put("real", TokenType.REAL);
			lookupTable.put("function", TokenType.FUNCTION);
			lookupTable.put("procedure", TokenType.PROCEDURE);
			lookupTable.put("begin", TokenType.BEGIN);
			lookupTable.put("end", TokenType.END);
			lookupTable.put("if", TokenType.IF);
			lookupTable.put("then", TokenType.THEN);
			lookupTable.put("else", TokenType.ELSE);
			lookupTable.put("while", TokenType.WHILE);
			lookupTable.put("do", TokenType.DO);
			lookupTable.put("not", TokenType.NOT);
			lookupTable.put("div", TokenType.DIV);
			lookupTable.put("mod", TokenType.MOD);
			lookupTable.put("and", TokenType.AND);
			lookupTable.put("or", TokenType.OR);
			
			// SYMBOLS
			lookupTable.put("=", TokenType.ASSIGN);
			lookupTable.put("<>", TokenType.NOTEQUAL);
			lookupTable.put("<", TokenType.LESSTHAN);
			lookupTable.put("<=", TokenType.LESSTHANEQUAL);
			lookupTable.put(">=", TokenType.GREATERTHANEQUAL);
			lookupTable.put(">", TokenType.GREATERTHAN);
			lookupTable.put("+", TokenType.PLUS);
			lookupTable.put("-", TokenType.MINUS);
			lookupTable.put("*", TokenType.MULTIPLY);
			lookupTable.put("/", TokenType.DIVIDE);
			lookupTable.put(":=", TokenType.ASSIGNOP);
			lookupTable.put("{", TokenType.LEFTCURLY);
			lookupTable.put("}", TokenType.RIGHTCURLY);
			lookupTable.put("(", TokenType.LEFTPARENTHESIS);
			lookupTable.put(")", TokenType.RIGHTPARENTHESIS);
			lookupTable.put(".", TokenType.DOT);
			lookupTable.put("[", TokenType.LEFTBRACKET);
			lookupTable.put("]", TokenType.RIGHTBRACKET);
			lookupTable.put(";", TokenType.SEMICOLON);
			lookupTable.put(":", TokenType.COLON);
		}
%}


/* Patterns */

doubleSymbols     = (<= | <> | >= | :=)
other        	  = .
letter        	  = [A-Za-z]

digit		  	  = [0-9]
digits		  	  = {digit} ({digit})*
optional_fraction = ({digit} \. {digits}*) | (\. {digits}+)
optional_exponent = \E (\+ | \-) digits*
num 			  = {digits} | {optional_exponent} | {optional_fraction}

id			  	  = {letter} ({letter} | {digit})*

whitespace    	  = [ \n\t\r]

%%
/* Lexical Rules */

{num}		{
				/** Prints the num found */
				System.out.println("Found a num: " + yytext());
				return (yytext());
			}

{id}     {
             /** Print out the word that was found. Initializes the hashmap and searches for the token. If found, prints the keyword. Otherwise, prints it as ID. */
			 SimpleScanner();
			 if (lookupTable.containsKey(yytext()))
			 {
				System.out.println("Keyword found: " + yytext());
			 }
			 else 
			 {
				System.out.println("ID found: " + yytext());
			 }
             return( yytext());
            }
					
            
{whitespace}  {  /* Ignore Whitespace */ 
                 return "";
              }

{other}    { 
			 /** Initializes the hashmap, searches for the element, and if found prints the symbol. Otherwise is an illegal char. */
			 SimpleScanner();
			 if (lookupTable.containsKey(yytext()))
			 {
				System.out.println("Symbol found: " + lookupTable.get(yytext()));
			 }
			 else 
			 {
				System.out.println("Illegal char: '" + yytext() + "' found.");
			 }
             return (yytext());
           }
		   
{doubleSymbols}    
		{ 
			/** Initializes the hashmap, searches for the element, and if found prints the symbol. Otherwise is an illegal char. */
			
			 SimpleScanner();
			 if (lookupTable.containsKey(yytext()))
			 {
				System.out.println("Symbol found: " + lookupTable.get(yytext()));
			 }
			 else 
			 {
				System.out.println("Illegal char: '" + yytext() + "' found.");
			 }
             return (yytext());
        }
		   