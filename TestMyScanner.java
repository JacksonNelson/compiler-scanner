import java.io.FileInputStream;
import java.io.InputStreamReader;

public class TestMyScanner
{

    public static void main( String[] args) 
    {
        String filename = args[0];		// Input comes from arg 1
        FileInputStream fis = null;
        try 
		{
            fis = new FileInputStream( filename);		// Make sure the file exists
        } 
		catch (Exception e ) { e.printStackTrace();}
        InputStreamReader isr = new InputStreamReader( fis);
        MyScanner scanner = new MyScanner( isr);
        String aToken = null;
        do
        {
            try 
			{
                aToken = scanner.nextToken();			// Try reading the next token
            }
            catch( Exception e) { e.printStackTrace();}
        } 
		while( aToken != null);							// Repeat while token exists
    }


}