Mini-Pascal Scanner - Jackson Nelson

This software will act as a scanner that identifies tokens from our Mini-Pascal language. This software utilizes the jFlex tool to create Java files that will scan 
input files for the appropriate tokens. Tokens are described in the grammar of this Mini-Pascal language and are composed of keywords, symbols, and more that are listed
in the grammar.txt file. 

This software will includes foo.jFlex, the document that performs the main operation of this scanner; TestMyScanner.java, the java class that calls the scanner;
Token.java, which defines a token object; TokenType.java, which defines all different token types; several sample documents that will be used to test the scanner, 
a text file listing keywords and symbols of the grammar, and any relevant documentation for this project (including an SDD, diagrams, state machines, etc.).

To run this software, open the terminal/command line and run TestMyScanner.java with your selected input file as the argument. Included in the folder are two text
documents, input.txt and input2.txt, which can be used to test the scanner. input.txt contains no errors, identifying the following: the - ID; while - KEYWORD; 
2.0 - NUM; < - LESSTHAN; = - ASSIGN; <= - LESSTHANEQUALS. input2.txt contains one error, and identifies the following: took - ID; for - ID; 3.5 - NUM;
< - LESSTHAN; > - GREATERTHAN; := - ASSIGNOP; : - COLON; ^ - ERROR; do - KEYWORD; while - KEYWORD. input3.txt tests the entire grammar.

Author: Jackson Nelson. nelsonj6@augsburg.edu